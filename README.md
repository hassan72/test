# README #

This is the material for the Git training at Humber College.

This repository contains:

- A copy of my .gitconfig
- A cheatsheet (what we are learning in class)
- Several .pdf files that cover in detail topics discussed in class

### How do I get it? ###


1. Go to your home directory

2. Open your command line and type: git clone https://bitbucket.org/ralvez/gitit

You are ready to go...

